package pl.wiktordolecki.crawler

import akka.actor.{Actor, ActorSystem}
import akka.event.Logging
import akka.pattern.pipe
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes.OK
import akka.stream.ActorMaterializer
import akka.util.ByteString
import org.mongodb.scala.bson.collection.immutable.Document
import org.mongodb.scala.{Completed, MongoClient, MongoCollection, MongoDatabase, Observer}
import pl.wiktordolecki.DotaApiClient

import scala.concurrent.ExecutionContextExecutor

class MatchInsertActor(val apiClient: DotaApiClient)
                      (implicit val client: MongoClient,
                       implicit val system: ActorSystem,
                       implicit val materializer: ActorMaterializer,
                       implicit val dispatcher: ExecutionContextExecutor) extends Actor {
  val LOG = Logging(context.system, this)

  val database: MongoDatabase = client.getDatabase("dota")
  val historyCollection: MongoCollection[Document] = database.getCollection("history")
  val detailsCollection: MongoCollection[Document] = database.getCollection("details")

  override def receive: Receive = {
    case resp@HttpResponse(OK, headers, entity, _) => {
      LOG.info("Details Request code: " + OK)

        entity.dataBytes
        .runFold(ByteString(""))(_ ++ _)
        .map(_.utf8String)
        .map((text) => {
          val doc = Document(text).get("result").get.asDocument()
          val mId: Long = doc.get("match_id").asInt64().longValue()
          DetailsRecord(mId, doc)
        })
        .foreach((detailsRec) => {
          self ! detailsRec
        })
    }

    case resp@HttpResponse(code, _, _, _) => {
      LOG.error("Details Request code: " + code)
      resp.discardEntityBytes()
    }

    case history: HistoryRecord => {
      val mId = history.mId
      LOG.info(s"History record $mId insert start")

      historyCollection.insertOne(history.doc).subscribe(new Observer[Completed] {
        override def onNext(result: Completed): Unit = LOG.info(s"History record $mId insert OK")

        override def onError(e: Throwable): Unit = LOG.error(e, s"Error on history record insert $mId")

        override def onComplete(): Unit = Unit
      })
    }

    case details: DetailsRecord => {
      val mId = details.mId
      LOG.info(s"Details record $mId insert start")

      detailsCollection.insertOne(details.doc).subscribe(new Observer[Completed] {
        override def onNext(result: Completed): Unit = LOG.info(s"Details record $mId insert OK")

        override def onError(e: Throwable): Unit = LOG.error(e, s"Error on details insert $mId")

        override def onComplete(): Unit = Unit
      })
    }

    case request: RequestMatchDetails => {
      val mId = request.mId

      apiClient.getMDFuture(mId) pipeTo self
    }

    case _ => {
      LOG.error("received unknown message")
    }
  }
}



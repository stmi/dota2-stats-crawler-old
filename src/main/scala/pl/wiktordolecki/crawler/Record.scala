package pl.wiktordolecki.crawler

import org.mongodb.scala.bson.BsonDocument

case class HistoryRecord(val mId: Long, val doc: BsonDocument)
case class DetailsRecord(val mId: Long, val doc: BsonDocument)
case class RequestMatchDetails(val mId: Long)

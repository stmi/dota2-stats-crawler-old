package pl.wiktordolecki.crawler

import akka.actor.{Actor, ActorRef, ActorSystem}
import akka.event.Logging
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes.OK
import akka.stream.{ActorMaterializer, ThrottleMode}
import akka.stream.scaladsl.Source
import akka.util.ByteString
import akka.pattern.pipe
import org.bson.BsonDocument
import org.mongodb.scala.bson.BsonValue
import org.mongodb.scala.bson.collection.immutable.Document
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.{MongoClient, MongoCollection, MongoDatabase, Observer}
import pl.wiktordolecki.DotaApiClient

import scala.concurrent.duration._
import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext

class MatchFilterActor(val apiClient: DotaApiClient, insertActor: ActorRef)
                      (implicit val client: MongoClient,
                       implicit val system: ActorSystem,
                       implicit val dispatcher: ExecutionContext,
                       implicit val materializer: ActorMaterializer) extends Actor {
  val LOG = Logging(context.system, this)

  val database: MongoDatabase = client.getDatabase("dota")
  val history: MongoCollection[Document] = database.getCollection("history")
  val details: MongoCollection[Document] = database.getCollection("details")

  override def receive: Receive = {
    case () => {
      LOG.info("Tick received, sending http request for match history")
      apiClient.getMHFuture pipeTo self
    }

    case HttpResponse(OK, _, entity, _) => {
      LOG.info("History Request response code: " + OK)
      entity.dataBytes
        .runFold(ByteString(""))(_ ++ _)
        .map(_.utf8String)
        .map(Document(_).get("result").get.asDocument())
        .map(_.get("matches").asArray().asScala.toList)
        .foreach((history: List[BsonValue]) => {
          Source.fromIterator(() => history.iterator)
            .throttle(1, 0.5 second, 1, ThrottleMode.shaping)
            .map(_.asDocument())
            .runForeach((doc: BsonDocument) => {
              self ! doc
            })
        })
    }

    case HttpResponse(code, _, entity, _) => {
      LOG.error("History Request response code: " + code)
      entity.discardBytes()
    }

    case doc: BsonDocument => {
      val mId: Long = doc.get("match_id").asInt64().longValue()

      LOG.info(s"Process match_id $mId")

      history.find(equal("match_id", mId)).subscribe(new Observer[Document] {
        var cntr = 0;

        override def onError(e: Throwable): Unit = LOG.error(e, "Error on history query")

        override def onComplete(): Unit = {
          if (cntr == 0) insertActor ! HistoryRecord(mId, doc)
        }

        override def onNext(result: Document): Unit = {
          cntr = cntr + 1;
        }
      })

      details.find(equal("match_id", mId)).subscribe(new Observer[Document] {
        var cntr = 0;

        override def onError(e: Throwable): Unit = LOG.error(e, "Error on details query")

        override def onComplete(): Unit = {
          if (cntr == 0) insertActor ! RequestMatchDetails(mId)
        }

        override def onNext(result: Document): Unit = {
          cntr = cntr + 1;
        }
      })
    }

    case _ => LOG.error("received unknown message")
  }
}
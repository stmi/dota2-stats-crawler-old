package pl.wiktordolecki

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream.ActorMaterializer

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContextExecutor, Future}

class DotaApiClient(val key: String)
                   (implicit val system: ActorSystem,
                    implicit val materializer: ActorMaterializer,
                    implicit val dispatcher: ExecutionContextExecutor) {

  val http = Http()
  val historyUrl =
    "https://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/v1?key=${dotaKey}&game_mode=1&min_players=10"
  val matchUrl =
    "https://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/v1?key=${dotaKey}&match_id=${matchId}"

  def getMHFuture = {
    val url = historyUrl.replace("${dotaKey}", key)

    http.singleRequest(HttpRequest(uri = url))
  }

  def getMatchHistory: HttpResponse = {
    val url = historyUrl.replace("${dotaKey}", key)

    val future: Future[HttpResponse] = http.singleRequest(HttpRequest(uri = url))
    Await.result(future, 10.second)
  }

  def getMDFuture(matchId: Long): Future[HttpResponse] = {
    val url = matchUrl.replace("${dotaKey}", key).replace("${matchId}", matchId.toString)

    http.singleRequest(HttpRequest(uri = url))
  }

  def getMatchDetails(matchID: Long): HttpResponse = {
    val url = matchUrl.replace("${dotaKey}", key).replace("${matchId}", matchID.toString)

    val future: Future[HttpResponse] = http.singleRequest(HttpRequest(uri = url))
    Await.result(future, 10.second)
  }
}

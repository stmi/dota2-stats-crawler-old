package pl.wiktordolecki.example

import java.text.SimpleDateFormat

import akka.Done
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ThrottleMode}
import akka.stream.scaladsl.Source
import org.mongodb.scala.{MongoClient, MongoCollection, MongoDatabase, Observer}
import org.mongodb.scala.bson.{BsonDocument, BsonValue}
import org.mongodb.scala.bson.collection.immutable.Document
import org.mongodb.scala.model.Filters.equal

import scala.io.{Source => ScalaSource}
import scala.collection.JavaConverters._
import scala.concurrent.duration._

object QueryMatchHistory extends App {

  implicit val system = ActorSystem("QuickStart")
  implicit val materializer = ActorMaterializer()
  implicit val dispatcher = system.dispatcher

  val matchHistory01: String = ScalaSource.fromResource("history/getMatchHistory_01.json").mkString

  val historyQueue = List(matchHistory01)
  val matchSrc = Source.fromIterator(() => historyQueue.iterator)
  val df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")

  val client = MongoClient()
  val database: MongoDatabase = client.getDatabase("dota")
  val collection: MongoCollection[Document] = database.getCollection("history")
  val details: MongoCollection[Document] = database.getCollection("details")

  matchSrc
    .throttle(1, 100.second, 1, ThrottleMode.shaping)
    .map((text: String) => {
      Document(text)
    })
    .mapConcat((doc: Document) => {
      val result = doc.get("result").get.asDocument()
      result.get("matches").asArray().asScala.toList.take(10)
    })
    .throttle(1, 1.second, 1, ThrottleMode.shaping)
    .map((v: BsonValue) => {
      v.asDocument()
    })
    .runForeach((doc: BsonDocument) => {
      val mId: Long = doc.get("match_id").asInt64().longValue()
      val rand = new scala.util.Random()
      collection.find(equal("match_id", mId + rand.nextInt(2))).subscribe(new Observer[Document] {
        var cntr = 0;

        override def onError(e: Throwable): Unit = println("Error")
        override def onComplete(): Unit = {
          if (cntr == 0) {
            println("No match documents found, action")
          } else {
            println("Document found, skip")
          }
        }
        override def onNext(result: Document): Unit = {
          cntr = cntr + 1;
        }
      })

      details.find(equal("match_id", mId + rand.nextInt(2))).subscribe(new Observer[Document] {
        var cntr = 0;

        override def onError(e: Throwable): Unit = println("Error")
        override def onComplete(): Unit = {
          if (cntr == 0) {
            println("No details documents found, action")
          } else {
            println("Document found, skip")
          }
        }
        override def onNext(result: Document): Unit = {
          cntr = cntr + 1;
        }
      })
    })
}

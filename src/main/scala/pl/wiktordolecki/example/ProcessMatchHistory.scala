package pl.wiktordolecki.example

import java.text.SimpleDateFormat
import java.util.Calendar

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ThrottleMode}
import org.mongodb.scala.{Completed, MongoClient, MongoCollection, MongoDatabase, Observer}
import akka.stream.scaladsl.{Source, _}
import akka.util.ByteString
import org.mongodb.scala.bson.BsonValue
import org.mongodb.scala.bson.collection.immutable.Document
import pl.wiktordolecki.DotaApiClient
import pl.wiktordolecki.config.ConfigFactory

import scala.io.{Source => ScalaSource}
import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._

object ProcessMatchHistory extends App {

  implicit val system: ActorSystem = ActorSystem("QuickStart")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val dispatcher: ExecutionContextExecutor = system.dispatcher

  val matchHistory01: String = ScalaSource.fromResource("history/getMatchHistory_01.json").mkString
  val matchHistory02: String = ScalaSource.fromResource("history/getMatchHistory_02.json").mkString
  val matchHistory03: String = ScalaSource.fromResource("history/getMatchHistory_03.json").mkString
  val matchHistory04: String = ScalaSource.fromResource("history/getMatchHistory_04.json").mkString
  val matchHistory05: String = ScalaSource.fromResource("history/getMatchHistory_05.json").mkString

  val historyQueue = List(matchHistory01, matchHistory02, matchHistory03, matchHistory04, matchHistory05)
  val matchSrc = Source.fromIterator(() => historyQueue.iterator)
  val df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")

  val client = MongoClient()
  val database: MongoDatabase = client.getDatabase("dota")
  val collection: MongoCollection[Document] = database.getCollection("history")
  val details: MongoCollection[Document] = database.getCollection("details")

  val config = ConfigFactory.readConfig
  val key = config.get.apiKeys.head.key

  val api = new DotaApiClient(key)

  matchSrc
    .throttle(1, 100.second, 1, ThrottleMode.shaping)
    .map((text: String) => {
      Document(text)
    })
    .mapConcat((doc: Document) => {
      val result = doc.get("result").get.asDocument()
      result.get("matches").asArray().asScala.toList
    })
    .throttle(1, 1.second, 1, ThrottleMode.shaping)
    .map((v: BsonValue) => {
      val matchDocument = v.asDocument()
      val insertObservable = collection.insertOne(matchDocument)
      val x = insertObservable.subscribe(new Observer[Completed] {
        override def onNext(result: Completed): Unit = Unit
        override def onError(e: Throwable): Unit = println(s"onError: $e")
        override def onComplete(): Unit = Unit
      })
      val mId: Long = matchDocument.get("match_id").asInt64().longValue()

      api.getMatchDetails(mId).entity.dataBytes
        .runFold(ByteString(""))(_ ++ _)
        .map(_.utf8String)
        .map(text => {
          Document(text).get("result").get.asDocument()
        })
        .foreach(mDet => {
          details.insertOne(mDet).subscribe(new Observer[Completed] {
            override def onNext(result: Completed): Unit = println("Match inserted")
            override def onError(e: Throwable): Unit = println(s"onError: $e")
            override def onComplete(): Unit = Unit
          })
        })

      mId
    })
    .runForeach((id : Long) => {
      Console.println(df.format(Calendar.getInstance.getTime) +
        " MatchId = " + id + " processed")
    })
    .onComplete(_ => system.terminate())
}

package pl.wiktordolecki.example

import java.util.Calendar
import java.text.SimpleDateFormat

import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl._
import org.bson.BsonValue
import org.mongodb.scala.bson.collection.immutable.Document

import scala.collection.JavaConverters._
import scala.concurrent.duration._
import scala.io.{Source => ScalaSource}

object AkkaStreamExample extends App {

  implicit val system = ActorSystem("QuickStart")
  implicit val materializer = ActorMaterializer()
  implicit val ec = system.dispatcher

  val matchHistory01: String = ScalaSource.fromResource("history/getMatchHistory_01.json").mkString
  val matchHistory02: String = ScalaSource.fromResource("history/getMatchHistory_02.json").mkString
  val matchHistory03: String = ScalaSource.fromResource("history/getMatchHistory_03.json").mkString

  val historyQueue = List(matchHistory01, matchHistory02, matchHistory03)

  val matchSrc = Source.fromIterator(() => historyQueue.iterator)
  val df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")

  matchSrc
    .throttle(1, 300.second, 1, ThrottleMode.shaping)
    .map((text : String) => Document(text))
    .mapConcat((doc: Document) => {
      Console.println(df.format(Calendar.getInstance.getTime) + " Process match history")
      val resultVal = doc.get("result").get.asDocument()
      val array = resultVal.get("matches").asArray().asScala
      array.toList
    })
    .throttle(1, 1.second, 1, ThrottleMode.shaping)
    .runForeach((v : BsonValue) => {
      val matchDocument = v.asDocument()
      Console.println(df.format(Calendar.getInstance.getTime) +
        " MatchId = " + matchDocument.get("match_id").asInt64().longValue())
    })
    .onComplete(_ => system.terminate())
}
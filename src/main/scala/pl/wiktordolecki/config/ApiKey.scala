package pl.wiktordolecki.config

class ApiKey
(
  val name: String,
  val key: String
) {
  override def toString = s"ApiKey(name=$name, key=$key)"
}

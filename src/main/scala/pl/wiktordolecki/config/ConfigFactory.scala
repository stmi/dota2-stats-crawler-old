package pl.wiktordolecki.config

import java.nio.file.Paths

import com.fasterxml.jackson.core.JsonParser.Feature
import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper

import scala.io.Source

object ConfigFactory {
  val CrawlerConfigName = ".dota2-stats-crawler"

  val mapper = new ObjectMapper() with ScalaObjectMapper
  mapper.registerModule(DefaultScalaModule)
  mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
  mapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)

  def readConfig: Option[Config] = {
    val configFile = Paths.get(System.getProperty("user.home"), CrawlerConfigName).toFile

    if (!configFile.exists || !configFile.isFile)
      None
    else {
      val source = Source.fromFile(configFile)
      val lines = try source.mkString finally source.close()

      Some(mapper.readValue(lines, classOf[Config]))
    }
  }
}

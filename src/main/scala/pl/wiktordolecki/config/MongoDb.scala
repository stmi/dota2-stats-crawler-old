package pl.wiktordolecki.config

class MongoDb
(
  val host: String,
  val database: String,
  val user: String,
  val password: String,
) {
  override def toString = s"MongoDbCredentials(user=$user, password=$password)"
}

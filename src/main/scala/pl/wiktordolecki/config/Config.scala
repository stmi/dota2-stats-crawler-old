package pl.wiktordolecki.config

class Config
(
  val apiKeys : List[ApiKey],
  val mongoDb: MongoDb
) {
  override def toString = s"Config(apiKeys=$apiKeys)"
}

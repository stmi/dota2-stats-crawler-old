package pl.wiktordolecki.dota2.crawler.entry

import com.amazonaws.services.lambda.runtime.{Context, LambdaLogger, RequestHandler}

object AWSLambdaCrawlerMain extends RequestHandler[Integer, String] {
  override def handleRequest(input: Integer, context: Context): String = {
    val logger = context.getLogger

    logger.log("Dota2 Stats Crawler packaged in lambda function")

    String.valueOf(input)
  }
}
package pl.wiktordolecki.dota2.crawler.entry

import akka.actor.{ActorSystem, Props}
import akka.stream.ActorMaterializer
import com.mongodb.ConnectionString
import org.mongodb.scala.connection.NettyStreamFactoryFactory
import org.mongodb.scala.{MongoClient, MongoClientSettings}
import pl.wiktordolecki.DotaApiClient
import pl.wiktordolecki.config.{ApiKey, ConfigFactory}
import pl.wiktordolecki.crawler.{MatchFilterActor, MatchInsertActor}

import scala.concurrent.duration._

object CrawlerMain extends App {
  implicit val system = ActorSystem("Crawler")
  implicit val dispatcher = system.dispatcher
  implicit val materializer = ActorMaterializer()

  val LOG = akka.event.Logging.getLogger(system, this)

  private val config = ConfigFactory.readConfig.get
  val apiKey: ApiKey = config.apiKeys.head

  val mongoUri = new ConnectionString(
    s"mongodb+srv://${config.mongoDb.user}:${config.mongoDb.password}" +
      s"@${config.mongoDb.host}/${config.mongoDb.database}?streamType=netty&ssl=true")

  val clientSession =
    MongoClientSettings.builder()
      .applyToSslSettings(builder => {
        builder.enabled(true)
      })
      .applyConnectionString(mongoUri)
      .streamFactoryFactory(NettyStreamFactoryFactory())
      .build()

  implicit val mongoClient: MongoClient = MongoClient(clientSession)

  LOG.info(mongoClient.toString)

  LOG.info("Config AppName:" + apiKey.name + " Key:" + apiKey.key)

  val apiClient = new DotaApiClient(apiKey.key)

  val insertActor = system.actorOf(Props(new MatchInsertActor(apiClient)), name = "insertActor")
  val filterActor = system.actorOf(Props(new MatchFilterActor(apiClient, insertActor)), name = "filterActor")

  LOG.info("STARTING CRAWLER")

  system.scheduler.schedule(5 second, 100 second, filterActor, ())
}

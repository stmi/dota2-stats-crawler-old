ThisBuild / name := "dota2_stats_crawler"
ThisBuild / version := "1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.12.6"
ThisBuild / organization := "pl.wiktordolecki"

lazy val dota2_stats_crawler = (project in file("."))
  .settings(
    javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint"),
    scalacOptions += "-target:jvm-1.8",

    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor" % "2.5.12",
      "com.typesafe.akka" %% "akka-testkit" % "2.5.12" % Test,
      "com.typesafe.akka" %% "akka-stream" % "2.5.12",
    ),

    libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.8.8",
    libraryDependencies += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.8.8",

    dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-databind" % "2.8.8",
    dependencyOverrides += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.8.8",

    libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.5" % Test,
    libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % Test,

    libraryDependencies += "io.netty" % "netty-all" % "4.1.25.Final",
    libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "2.3.0",

    libraryDependencies += "com.typesafe.akka" %% "akka-http-core" % "10.1.1" ,
    libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.1.1",
    libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % "10.1.1",
    libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.1",
    libraryDependencies += "com.typesafe.akka" %% "akka-http-jackson" % "10.1.1",
    libraryDependencies += "com.typesafe.akka" %% "akka-http-xml" % "10.1.1",

    libraryDependencies += "com.amazonaws" % "aws-lambda-java-core" % "1.2.0"
  )
  .settings(
    assemblyJarName in assembly := "dota2-stats-crawler.jar",
    mainClass in assembly := Some("pl.wiktordolecki.dota2.crawler.entry.CrawlerMain")
  )

# Dota2 Stats Crawler

Crawler collecting Dota2 game data implemented in Scala, using MongoDb as storage.

GitLab Page: https://stmi.gitlab.io/dota2-stats-crawler

## Configuration

Application is looking for config file in

```
${HOME}/.dota2-stats-crawler
```

Expected layout of file is json containing following fields

```
{
    "apiKeys" : [
        {
            "name" : "name-of-your-steam-app"
            "key" : "1234567890ABCDEF1234567890ABCDEF"
        }
    ],
    "mongoDb" : {
        "host" : "your.db.host",
        "database" : "your-database-name",
        "user" : "your-user-name",
        "password" : "your-password"
    }
}
```

## Other resources

1. Obtaining Steam API Key: https://steamcommunity.com/dev/apikey